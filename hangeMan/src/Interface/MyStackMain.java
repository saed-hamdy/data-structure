package Interface;

import java.util.Scanner;

import eg.edu.alexu.csd.datastructure.stack.cs34.MyStack;

public class MyStackMain {
    private static Scanner in;

    public static void printLine() {
        System.out.println("======================================================================");
    }

    public static void printOptions() {
        Print("1: Push\n" + "2: Pop\n" + "3: Peek\n" + "4: Get size\n" + "5: Check if empty\n" + "6:close program");

        printLine();
    }

    public static void Print(String x) {

        System.out.println(x);
    }

    public static void main(String[] args) {
        in = new Scanner(System.in);
        MyStack saeed = new MyStack();

        int n;
        boolean flag = true;

        while (flag) {
            printOptions();

            if (in.hasNextInt()) {
                n = in.nextInt();
            } else {
                in.next();
                n = 0;
            }

            switch (n) {
            case 1:
                Print("enrer opject :");
                saeed.push(in.next());
                printLine();
                break;
            case 2:
                Print("what you peeked is:" + saeed.peek().toString());
                printLine();
                break;
            case 3:
                Print("what you peeked is:" + saeed.pop().toString());
                printLine();
                break;
            case 4:
                Print("Size is:  " + (int) saeed.size());
                printLine();
                break;
            case 5:
                Print(saeed.isEmpty() ? "yes its Empty" : "No ,its not empty");
                printLine();
                break;
            case 6:
                Print("======================= Thanks==========================");
                flag = false;
            }
            in.nextLine();
        }
    }
}