package Interface;

import java.util.Scanner;

import eg.edu.alexu.csd.datastructure.linkedList.cs34.Polynomial;

//import javax.xml.ws.FaultAction;

public class MyPoly {
    public static void printLine() {
        System.out.println("======================================================================");
    }

    static Scanner in = new Scanner(System.in);

    public static void Print(String x) {

        System.out.println(x);
    }

    public static char scan() {
        String c = in.next();
        char s = c.charAt(0);
        while (c.length() != 1 || (s != 'A' && s != 'B' && s != 'C' && s != 'R')) {
            Print("Unvalid input,  Please Enter again");
            c = in.next();
            s = c.charAt(0);
        }
        return s;
    }

    public static void main(String[] args) {

        boolean ok = true;
        Polynomial p = new Polynomial();
        while (ok) {

            Print("1- Set a polynomial variable\n" + "2- Print the value of a polynomial variable\n"
                    + "3- Add two polynomials\n" + "4- Subtract two polynomials\n" + "5- Multiply two polynomials\n"
                    + "6- Evaluate a polynomial at some point\n" + "7- Clear a polynomial variable\n"
                    + "8- Exit Program");
            printLine();
            int n;
            // String rounds;
            if (in.hasNextInt()) {
                n = in.nextInt();
            } else {
                in.next();
                n = 0;
            }
            char a1 = 0;
            char a2 = 0;
            char list = 0;
            switch (n) {
            case 1:
                // d:
                Print("Insert the variable name : A, B or C");
                list = scan();
                // goto d;
                while (list == 'R') {
                    Print("Cannot set R ,Please retry");
                    list = scan();
                }
                int i = 0;
                String line = new String();
                int[][] array = new int[100][2];
                Print("Insert the polynomial terms in the form :\n" + "(coeff1 , exponent1 ), (coeff2 , exponent2 ),");
                line = in.next();
                // @SuppressWarnings("resource")
                Scanner input = new Scanner(line).useDelimiter("\\(|,|\\)|\\s");
                // Scanner ali = new
                // Scanner(line).useDelimiter("\\(|,|\\)|\\s"); // ignoring
                // while (ali.hasNext()) {
                while (input.hasNext()) {
                    if (input.hasNextInt()) {
                        array[i][0] = input.nextInt();
                        array[i][1] = input.nextInt();
                        i++;
                    } else
                        input.next();
                }
                int[][] arr = new int[i][2];
                for (int k = 0; k < i; k++) {
                    arr[k] = array[k];

                }
                try {
                    p.setPolynomial(list, arr);
                } catch (Exception op) {
                    Print("un valid inputs");
                }
                printLine();
                break;
            case 2:
                Print("Insert the variable name : A, B or C");
                list = scan();
                if (p.choose_plo(list).isEmpty()) {
                    Print("not set");
                } else
                    p.print(list);
                printLine();
                break;
            case 3:
                Print("Insert the first polynomial");
                a1 = scan();
                Print("Insert the second polynomial");
                a2 = scan();
                if (p.choose_plo(a1).isEmpty() || p.choose_plo(a2).isEmpty()) {
                    Print("Error Not set polynomial");
                } else {
                    p.add(a1, a2);
                    Print("Sum :");
                    p.print('R');
                }
                printLine();
                break;
            case 4:
                Print("Insert the first polynomial");
                a1 = scan();
                Print("Insert the second polynomial");
                a2 = scan();
                if (p.choose_plo(a1).isEmpty() || p.choose_plo(a2).isEmpty()) {
                    Print("Error Not set polynomial");
                } else {
                    p.subtract(a1, a2);
                    Print("Result :");
                    p.print('R');
                }
                printLine();
                break;
            case 5:
                Print("Insert the first polynomial");
                a1 = scan();
                Print("Insert the second polynomial");
                a2 = scan();
                if (p.choose_plo(a1).isEmpty() || p.choose_plo(a2).isEmpty()) {
                    Print("Error Not set polynomial");
                } else {
                    p.multiply(a1, a2);
                    Print("Result :");
                    p.print('R');
                }
                printLine();
                break;
            case 6:
                Print("Insert the variable name : A, B or C to evaluate");
                a2 = scan();
                Print("E  nter the value");
                if (in.hasNextInt()) {
                    int value = in.nextInt();
                    if (p.choose_plo(a2).isEmpty()) {
                        Print("Error Empty polynomial");
                    } else
                        Print("The value =" + (int) p.evaluatePolynomial(a2, value));
                    printLine();
                } else
                    Print("Not valid");
                break;
            case 7:
                Print("Insert the polynomial to clear");
                a2 = scan();
                if (p.choose_plo(a2).isEmpty()) {
                    Print("Error Empty polynomial");
                } else {
                    p.clearPolynomial(a2);
                }
                printLine();
                break;
            case 8:
                ok = false;
                Print("                                Thank u                             \n"
                        + "============================!!!!!!!!!!!!!!!=========================");
                break;
            default:
                Print("Not a valid action");
                printLine();

            }
        }
    }

}