package eg.edu.alexu.csd.oop.calculator.cs34;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import eg.edu.alexu.csd.oop.calculator.Calculator;

public class MyCalculator implements Calculator {
    
    
    private  String[] str = new String[6];
    private int cnt=4, f = 4, r = 0, size=0;
    private ScriptEngineManager mgr = new ScriptEngineManager();
    private ScriptEngine engine = mgr.getEngineByName("JavaScript");

    @Override
    public void input(String s) {
        // TODO Auto-generated method stub
        
       // cnt=counter (1);
        if(cnt!=f){
            size-=(f-cnt+5)%5;
            f=cnt;
        }
        cnt=f=(f+1)%5;
        str[f]=s;   
        if(size<5)size++;
        else {
            r=(r+1)%5;
        }
        
    }

    @Override
    public String getResult() {
        try{
        
       // in = new Scanner(System.in);
        //String foo = "40+2";
        //foo=in.next();
        String rs=engine.eval(str[cnt]).toString();
        String zero  = engine.eval("2/0").toString();
        if(rs!=zero)
            return(rs);
        else throw new RuntimeException();
        }
        catch( Exception p){
             return null ;         
        }
        
       
    }

    @Override
    public String current() {
        // TODO Auto-generated method stub
        if (size>0)
            return str[cnt];
        else 
            return null;
    }

    @Override
    public String prev() {
        // TODO Auto-generated method stub
       // cnt=counter(-1);
        if(cnt!=r){
            cnt=(cnt+4)%5;
            return str[cnt];
        }else{
            return null;
        }
            
    }

    @Override
    public String next() {
        // TODO Auto-generated method stub
        if(cnt==f)
            return null;
        cnt=(cnt+1)%5;
        return str[cnt];
    }

    @Override
    public void save() {
        // TODO Auto-generated method stub
        try { // Catch errors in I/O if necessary.
            // Open a file to write to, named SavedObj.sav.
            FileOutputStream saveFile = new FileOutputStream("Save.sav");

            // Create an ObjectOutputStream to put objects into save file.
            ObjectOutputStream save = new ObjectOutputStream(saveFile);

            // Now we do the save.
            save.writeObject(str);
            save.writeObject(f);
            save.writeObject(r);
            save.writeObject(size);
            
            // Close the file.
            save.close(); // This also closes saveFile.
        } catch (Exception exc) {
            exc.printStackTrace(); // If there was an error, print the info.
        }
  
    }

    @Override
    public void load() {
        // TODO Auto-generated method stub
        try {
            FileInputStream saveFile = new FileInputStream("save.sav");
            ObjectInputStream save = new ObjectInputStream(saveFile);
            str = (String[]) save.readObject();
            cnt = f =(Integer)save.readObject();
            r = (Integer)save.readObject();
            size = (Integer)save.readObject();
            
            

        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }

}
