package eg.edu.alexu.csd.datastructure.linkedList.cs34;

//import javax.management.RuntimeErrorException;
//import javax.sql.rowset.spi.TransactionalWriter;

//import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;
import eg.edu.alexu.csd.datastructure.linkedList.IPolynomialSolver;

public class Polynomial implements IPolynomialSolver {
    public SingleLinkedList a = new SingleLinkedList();
    public SingleLinkedList b = new SingleLinkedList();
    public SingleLinkedList c = new SingleLinkedList();
    public SingleLinkedList R = new SingleLinkedList();
    int i;

    public void check(int[][] x) {
        if (x[0][1] < 0)
            throw new RuntimeException();
        for (int k = 0; k < x.length - 1; k++) {
            if (x[k][1] <= x[k + 1][1] || x[k][0] == 0 || x[k + 1][0] == 0 || x[k][1] < 0) {
                throw new RuntimeException();
            }
        }
    }

    public SingleLinkedList choose_plo(char poly) {

        switch (poly) {
        case 'A':
            return a;
        case 'B':
            return b;
        case 'C':
            return c;
        case 'R':
            return R;
        default:
            throw new RuntimeException();
        }
    }

    public int[][] toArray(SingleLinkedList x) {
        int[][] ala = new int[x.size()][2];
        for (i = 0; i < x.size(); i++) {
            ala[i] = (int[]) x.get(i);
        }
        return ala;
    }

    @Override
    public void setPolynomial(char poly, int[][] terms) {

        if (poly == 'R')
            throw new RuntimeException();
        check(terms);
        // if(terms == null)
        // throw new RuntimeException();
        choose_plo(poly).clear();

        for (i = 0; i < terms.length; i++) {

            choose_plo(poly).add(terms[i]);

        }
    }

    @Override
    public String print(char poly) {
        // TODO Auto-generated method stub
        if (choose_plo(poly).head == null)
            return null;
        int[][] pr;
        pr = toArray(choose_plo(poly));
        String s = "";
        for (i = 0; i < choose_plo(poly).size(); i++) {
            // sum=choose_plo(poly).get(i)
            // System.out.println(pr[0]+ "x^"+ pr[1]);
            if (pr[i][1] != 0) {
                if (pr[0][0] < 0 && i == 0) {
                    s = s.concat("-");
                }
                if (Math.abs(pr[i][0]) != 1) {
                    s = s.concat(String.format("%s", Math.abs(pr[i][0])));
                }
                s = s.concat("x");
                if (pr[i][1] != 1) {
                    s = s.concat("^");
                    s = s.concat(String.format("%s", Math.abs(pr[i][1])));
                }
            } else {
                if (pr[i][0] != 0) {
                    s = s.concat(String.format("%s", Math.abs(pr[i][0])));
                }

            }
            if (i != choose_plo(poly).size() - 1) {
                if (pr[i + 1][0] > 0) {
                    s = s.concat("+");
                } else
                    s = s.concat("-");

            }
        }

        System.out.println(s);
        return s;
    }

    @Override
    public void clearPolynomial(char poly) {
        // TODO Auto-generated method stub
        if (choose_plo(poly).head == null)
            throw new RuntimeException();
        choose_plo(poly).clear();
    }

    @Override
    public float evaluatePolynomial(char poly, float value) {
        // TODO Auto-generated method stub
        float sum = 0;
        if (choose_plo(poly).head == null)
            throw new RuntimeException();
        int[][] s;
        s = toArray(choose_plo(poly));
        for (i = 0; i < choose_plo(poly).size(); i++) {
            sum += s[i][0] * Math.pow((double) value, (double) s[i][1]);
        }
        return sum;
    }

    @Override
    public int[][] add(char poly1, char poly2) {
        if (choose_plo(poly1).head == null || choose_plo(poly2).head == null)
            throw new RuntimeException();
        int[][] h1;
        h1 = toArray(choose_plo(poly1));
        int[][] h2;
        h2 = toArray(choose_plo(poly2));
        int[][] sum = new int[100][2];
        int h11 = 0, h22 = 0;
        i = 0;
        while (h11 < h1.length && h22 < h2.length) {
            if (h1[h11][1] > h2[h22][1]) {
                sum[i] = h1[h11];
                h11++;
                // h1.next
            } else if (h1[h11][1] < h2[h22][1]) {
                sum[i] = h2[h22];
                h22++;
            } else {
                sum[i][0] = h1[h11][0] + h2[h22][0];
                sum[i][1] = h1[h11][1];
                h11++;
                h22++;
            }
            i++;

        }
        if (h11 == h1.length) {
            if (h22 < h2.length) {
                for (int k = h22; k < h2.length; k++) {
                    sum[i] = h2[k];
                    i++;
                }
            }
        } else if (h22 == h2.length) {
            for (int k = h11; k < h1.length; k++) {
                sum[i] = h1[k];
                i++;
            }
        }
        int[][] s = new int[i][2];
        R.clear();
        for (int k = 0; k < i; k++) {
            s[k] = sum[k];
            R.add(sum[k]);
        }
        int[][] y = new int[1][2];
        y[0][0] = 0;
        y[0][1] = 0;
        if (i == 0) {
            R.add(y);
            return y;
        }

        return s;
    }

    public int[][] subtract(char poly1, char poly2) {
        // TODO Auto-generated method stub
        if (choose_plo(poly1).head == null || choose_plo(poly2).head == null)
            throw new RuntimeException();
        if (poly1 == poly2) {
            int[][] u = new int[1][2];
            u[0][0] = 0;
            u[0][1] = 0;
            return u;
        }
        int[] z = new int[2];
        Polynomial q = new Polynomial();
        q.a = choose_plo(poly1);
        q.b = choose_plo(poly2);
        for (int k = 0; k < choose_plo(poly2).size(); k++) {
            z = (int[]) q.b.get(k);
            z[0] *= (-1);
            q.b.set(k, z);
        }
        int[][] s;
        s = q.add(poly1, poly2);
        R = q.R;
        return s;
    }

    @Override
    public int[][] multiply(char poly1, char poly2) {
        // TODO Auto-generated method stub

        int[][] h1;
        h1 = toArray(choose_plo(poly1));
        int[][] h2;
        h2 = toArray(choose_plo(poly2));
        int[][][] sum = new int[h1.length][h2.length][2];
        // int h11 = 0, h22 = 0,coun=0;
        for (i = 0; i < h1.length; i++) {
            for (int k = 0; k < h2.length; k++) {
                sum[i][k][0] = h1[i][0] * h2[k][0];
                sum[i][k][1] = h1[i][1] + h2[k][1];
            }
        }
        Polynomial s = new Polynomial();
        s.setPolynomial('A', sum[0]);
        for (i = 1; i < h1.length; i++) {
            s.setPolynomial('B', sum[i]);
            s.setPolynomial('A', s.add('A', 'B'));

        }
        int[][] m = s.toArray(s.a);
        R.clear();
        for (i = 0; i < m.length; i++) {
            R.add(m[i]);
        }
        return m;
    }

}