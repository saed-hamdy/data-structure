package eg.edu.alexu.csd.datastructure.linkedList.cs34;
//package eg.edu.alexu.csd.datastructure.linkedList.cs34;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

public class DoubleLinkedList implements ILinkedList {
    public Node head = null;
    Node tail = null;
    int size = 0, i = 0;

    public void add(int index, Object element) {
        if (element == null) {
            throw new RuntimeException();
        }
        Node temp = new Node();
        if (index > size || index < 0) {
            throw new RuntimeException("not a node");
        } else {
            if (index == 0) {
                temp.value = element;
                temp.next = head;
                head = temp;
                if (size == 0) {
                    tail = head;
                }
                size++;

            } else if (index == size) {
                add(element);
            } else {
                Node x = head;
                for (i = 0; i < index - 1; i++) {
                    x = x.next;
                }
                temp.value = element;
                temp.next = x.next;
                x.next = temp;
                size++;
            }
        }

    }

    public void add(Object element) {
        // TODO Auto-generated method stub
        // throw new RuntimeException(element.toString());
        if (element == null) {
            throw new RuntimeException();
        }
        Node temp = new Node();
        temp.value = element;
        if (head == null) {
            head = temp;
            tail = temp;
            size++;
        } else {
            tail.next = temp;
            tail = temp;
            size++;
        }
    }

    @Override
    public Object get(int index) {
        // TODO Auto-generated method stub
        // if (index == null){throw new RuntimeException();}
        Node x = head;

        if (index >= size || index < 0) {
            throw new RuntimeException("");

        } else {
            for (i = 0; i < index; i++) {
                x = x.next;
            }
            return x.value;
        }

    }

    @Override
    public void set(int index, Object element) {
        // TODO Auto-generated method stub
        if (element == null) {
            throw new RuntimeException();
        }
        Node x = head;
        if (index >= size || index < 0) {
            throw new RuntimeException();
        } else {
            for (i = 0; i < index; i++) {
                x = x.next;
            }
            x.value = element;
        }

    }

    public void clear() {
        // TODO Auto-generated method stub
        head = null;
        size = 0;
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        if (size == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new RuntimeException();
        }
        if (index == 0) {
            head = head.next;
            size--;
        } else {
            Node x = head;
            for (i = 0; i < index - 1; i++) {
                x = x.next;
            }
            Node temp = x.next;
            x.next = temp.next;
            size--;
        }
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return size;
    }

    public ILinkedList sublist(int fromIndex, int toIndex) {
        // TODO Auto-generated method stub
        ILinkedList y = new SingleLinkedList();
        Node x = head;
        for (i = 0; i < fromIndex; i++) {
            x = x.next;
        }
        for (i = 0; i <= toIndex - fromIndex; i++) {
            y.add(x.value);
            x = x.next;
        }
        return y;
    }

    @Override
    public boolean contains(Object o) {
        // TODO Auto-generated method stub
        if (o == null) {
            throw new RuntimeException();
        }
        Node x = head;
        if (size == 0) {
            return false;
        }
        for (i = 0; i < size; i++) {
            if (x.value.equals(o)) {
                return true;
            }
            x = x.next;
        }
        return false;
    }
}