package eg.edu.alexu.csd.datastructure.maze.cs34;

public class node {
    int x;
    int y;
    char value;
    node parents;

    public node(int s1, int s2, char s3) {
        x = s1;
        y = s2;
        value = s3;
    }

}