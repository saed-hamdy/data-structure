package eg.edu.alexu.csd.datastructure.maze.cs34;

import java.io.File;
import java.util.Scanner;

import eg.edu.alexu.csd.datastructure.maze.IMazeSolver;
import eg.edu.alexu.csd.datastructure.queue.cs34.MyQueue;
import eg.edu.alexu.csd.datastructure.stack.cs34.MyStack;

public class MyMaze implements IMazeSolver {
    int[] start = new int[2];

    // node start ;
    int end_x;
    int end_y;
    char[][] map1 = new char[1][];
    boolean[][] visted;

    public boolean valid(int x, int y) {
        if (x < 0 || y < 0 || x > end_x || y > end_y || map1[x][y] == '#' || visted[x][y]) {

            return false;
        } else if (map1[x][y] == '.' || map1[x][y] == 'E') {
            visted[x][y] = true;
            return true;

        }
        throw new RuntimeException();
    }

    public node[][] GetMap(File maze) {
        int nu_Of_start = 0;
        int nu_of_ends = 0;
        Scanner in;
        String x;
        Scanner nn;
        int x1;
        int x2;
        node[][] map;
        try {
            // int[] start = new int[2];
            in = new Scanner(maze);
            x = in.nextLine();
            nn = new Scanner(x);
            x1 = nn.nextInt();
            x2 = nn.nextInt();

        } catch (Exception p) {
            throw new RuntimeException("file not found ");
        }
        end_x = x1 - 1;
        end_y = x2 - 1;
        map1 = new char[x1][x2];
        visted = new boolean[x1][x2];
        map = new node[x1][x2];
        for (int i = 0; i < x1; i++) {
            map1[i] = in.nextLine().toCharArray();

            if (map1[i].length != x2) {
                throw new RuntimeException();
            } else {
                for (int j = 0; j < x2; j++) {
                    map[i][j] = new node(i, j, map1[i][j]);
                    visted[i][j] = false;
                }
            }
        }
        if (in.hasNextLine()) {
            throw new RuntimeException("has line");
        }
        for (int i = 0; i < x1; i++) {
            for (int j = 0; j < x2; j++) {
                if (map1[i][j] == 'E') {
                    nu_of_ends++;
                } else if (map1[i][j] == 'S') {
                    nu_Of_start++;
                    start[0] = i;
                    start[1] = j;
                }

            }
        }
        if (nu_of_ends == 0 || nu_Of_start != 1) {
            throw new RuntimeException("wrong file ");
        }
        return map;
    }

    @Override
    public int[][] solveBFS(File maze) {
        // TODO Auto-generated method stub

        MyQueue q = new MyQueue();
        node[][] map = GetMap(maze);
        node end;
        int x = start[0];
        int y = start[1];
        q.enqueue(map[x][y]);
        visted[x][y] = true;
        while (true) {
            if (q.isEmpty()) {
                return null;
            } else {
                node parent = (node) q.dequeue();
                x = parent.x;
                y = parent.y;
                if (valid(x - 1, y)) {
                    map[x - 1][y].parents = parent;
                    q.enqueue(map[x - 1][y]);
                    if (map[x - 1][y].value == 'E') {
                        end = map[x - 1][y];
                        break;
                    }
                }
                if (valid(x, y + 1)) {
                    map[x][y + 1].parents = parent;
                    q.enqueue(map[x][y + 1]);
                    if (map[x][y + 1].value == 'E') {
                        end = map[x][y + 1];
                        break;
                    }
                }
                if (valid(x + 1, y)) {
                    map[x + 1][y].parents = parent;
                    q.enqueue(map[x + 1][y]);
                    if (map[x + 1][y].value == 'E') {
                        end = map[x + 1][y];
                        break;
                    }
                }
                if (valid(x, y - 1)) {
                    map[x][y - 1].parents = parent;
                    q.enqueue(map[x][y - 1]);
                    if (map[x][y - 1].value == 'E') {
                        end = map[x][y - 1];
                        break;
                    }
                }

            }
        }
        MyQueue path = new MyQueue();
        path.enqueue(end);
        // boolean star = false;
        while (true) {
            path.enqueue(end.parents);
            end = end.parents;
            if (end.value == 'S') {
                break;
            }
        }
        int length = path.size();
        int[][] BFS = new int[length][2];
        for (int i = 0; i < length; i++) {
            node k = (node) path.dequeue();
            BFS[length - 1 - i][0] = k.x;
            BFS[length - 1 - i][1] = k.y;

        }
        return BFS;

    }

    @Override
    public int[][] solveDFS(File maze) {
        // TODO Auto-generated method stub
        MyStack stack = new MyStack();
        node[][] map = GetMap(maze);
        boolean end = false;
        boolean took = false;
        int x = start[0];
        int y = start[1];
        visted[x][y] = true;
        stack.push(map[x][y]);
        while (!end) {
            took = false;
            if (!stack.isEmpty()) {
                x = ((node) stack.peek()).x;
                y = ((node) stack.peek()).y;
                if (valid(x - 1, y)) {
                    stack.push(map[x - 1][y]);
                    took = true;
                    if (((node) stack.peek()).value == 'E') {
                        end = true;
                    }

                } else if (valid(x, y + 1)) {
                    stack.push(map[x][y + 1]);
                    took = true;
                    if (((node) stack.peek()).value == 'E') {
                        end = true;
                    }
                } else if (valid(x + 1, y)) {
                    stack.push(map[x + 1][y]);
                    took = true;
                    if (((node) stack.peek()).value == 'E') {
                        end = true;
                    }
                } else if (valid(x, y - 1)) {
                    stack.push(map[x][y - 1]);
                    took = true;
                    if (((node) stack.peek()).value == 'E') {
                        end = true;
                    }
                }
                if (!took && stack.size() > 0) {
                    stack.pop();
                } else if (stack.isEmpty()) {
                    return null;
                } else {
                    took = false;
                }
            } else {
                return null;
            }
        }
        int length = stack.size();
        int[][] path = new int[stack.size()][2];
        for (int i = 0; i < length; i++) {
            path[length - 1 - i][0] = ((node) stack.peek()).x;
            path[length - 1 - i][1] = ((node) stack.pop()).y;
        }

        return path;
    }

}