package eg.edu.alexu.csd.datastructure.queue.cs34;

import eg.edu.alexu.csd.datastructure.linkedList.cs34.Node;
import eg.edu.alexu.csd.datastructure.linkedList.cs34.SingleLinkedList;
import eg.edu.alexu.csd.datastructure.queue.ILinkedBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;

public class MyQueue implements IQueue, ILinkedBased {

    // Object top =new Object();
    SingleLinkedList list = new SingleLinkedList();

    @Override
    public void enqueue(Object item) {
        // TODO Auto-generated method stub
        list.add(item);
    }

    @Override
    public Object dequeue() {
        // TODO Auto-generated method stub
        Node temp = list.head;
        list.remove(0);
        // top=list.head;
        return temp.value;

    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return list.isEmpty();
    }

    public Object top() {
        return list.head;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return list.size();
    }

}