package eg.edu.alexu.csd.datastructure.queue.cs34;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyTests {

    @Test
    public void testArray1() {
        MyQueuueAR y = new MyQueuueAR(3);

        y.enqueue(1);
        y.enqueue(5);
        y.enqueue(6);

        assertEquals(y.dequeue(), 1);
        assertEquals(y.dequeue(), 5);
        assertEquals(y.dequeue(), 6);
        assertEquals(y.size, 0);
    }

    public void testArray2() {
        MyQueuueAR y = new MyQueuueAR(100);
        for (int i = 0; i < 100; i++) {
            y.enqueue(1);
            assertEquals(y.size, i + 1);
        }
        for (int i = 0; i < 100; i++) {
            assertEquals(y.size, 100 - i);
            y.dequeue();

        }
    }

    public void testlinked_List1() {
        MyQueuueAR y = new MyQueuueAR(100);
        for (int i = 0; i < 100000; i++) {
            y.enqueue(1);
            y.dequeue();
        }
        assertEquals(y.size, 0);
    }

    public void testlinked_List2() {
        MyQueuueAR y = new MyQueuueAR(100);
        y.enqueue(1);
        y.enqueue(5);
        y.enqueue(6);

        assertEquals(y.dequeue(), 1);
        assertEquals(y.dequeue(), 5);
        assertEquals(y.size, 0);
        assertEquals(y.dequeue(), 6);
        assertEquals(y.size, 0);

    }

}