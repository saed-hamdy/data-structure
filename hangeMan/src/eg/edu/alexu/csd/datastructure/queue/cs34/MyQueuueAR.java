package eg.edu.alexu.csd.datastructure.queue.cs34;

import eg.edu.alexu.csd.datastructure.queue.IArrayBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;

public class MyQueuueAR implements IQueue, IArrayBased {

    int n;
    Object[] array;
    Object top;

    public MyQueuueAR(Integer n) {
        // TODO Auto-generated constructor stub
        this.n = n;
        if (n < 0) {
            throw new RuntimeException();
        }
        array = new Object[n];

    }

    int size = 0, f = 0, r = 0;

    @Override
    public void enqueue(Object item) {
        // TODO Auto-generated method stub
        if (size == n) {
            throw new RuntimeException();
        } else {
            array[r] = item;
            r = (r + 1) % n;
            size++;
            if (size > 0){
                top = array[f];
            }
        }

    }

    @Override
    public Object dequeue() {
        // TODO Auto-generated method stub
        if (size == 0) {
            throw new RuntimeException();
        } else {
            Object temp = array[f];
            f = (f + 1) % n;
            size--;
            if (size > 0){
                top = array[f];
            }
            return temp;
        }
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return size == 0;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return size;
    }

}