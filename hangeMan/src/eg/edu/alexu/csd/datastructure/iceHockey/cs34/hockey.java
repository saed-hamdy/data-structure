package eg.edu.alexu.csd.datastructure.iceHockey.cs34;

import java.awt.Point;
//import javax.xml.soap.MimeHeader;
import eg.edu.alexu.csd.datastructure.iceHockey.IPlayersFinder;

public class hockey implements IPlayersFinder {
    char loc[][];
    int[][] points = new int[100][];
    Point p[];
    int j, i, k = 0, count = 0;
    int minX, minY, maxX, maxY;

    public void get_location(int x, int y, int team) {
        if (loc[y][x] - '0' == team) {
            loc[y][x]++;
            count++;
            if (2 * (x + 1) > maxX) {
                maxX = 2 * (x + 1);
            }
            if (2 * (x) < minX) {
                minX = 2 * (x);
            }
            if (2 * (y + 1) > maxY) {
                maxY = 2 * (y + 1);
            }
            if (2 * (y) < minY) {
                minY = 2 * (y);
            }
            if (x < loc[0].length - 1) {
                get_location(x + 1, y, team);
            }
            if (x >= 1) {
                get_location(x - 1, y, team);
            }
            if (y < loc.length - 1) {
                get_location(x, y + 1, team);
            }
            if (y >= 1) {
                get_location(x, y - 1, team);
            }
        }
    }

    public void sort_points() {
        int[] temp = new int[2];
        int index = 0;
        for (i = 0; i < k - 1; i++) {
            index = i;
            for (j = i; j < k; j++) {
                if (points[j][0] < points[index][0]
                        || (points[j][0] == points[index][0] && points[j][1] < points[index][1])) {
                    index = j;
                }
            }
            temp = points[i];
            points[i] = points[index];
            points[index] = temp;
        }
        p = new Point[k];
        for (i = 0; i < k; i++) {
            p[i] = new Point(points[i][0], points[i][1]);
        }
    }

    @Override
    public Point[] findPlayers(String[] photo, int team, int threshold) {
        // TODO Auto-generated method stub
        if (photo.length == 0) {
            return new Point[0];
        } else {
            points = new int[100][];
            k = 0;
            loc = new char[photo.length][photo[0].length()];
            for (i = 0; i < photo.length; i++) {
                loc[i] = photo[i].toCharArray();
            }
            for (i = 0; i < photo.length; i++) {
                for (j = 0; j < loc[i].length; j++) {
                    if (loc[i][j] - '0' == team) {
                        count = 0;
                        minX = j * 2;
                        minY = i * 2;
                        maxX = (j + 1) * 2;
                        maxY = (i + 1) * 2;
                        get_location(j, i, team);
                        // Point l= new Point((minX+maxX)/2, (minY+maxY)/2);
                        if (count * 4 >= threshold) {
                            points[k] = new int[] { (minX + maxX) / 2, (minY + maxY) / 2 };
                            // p[k]=new Point((minX+maxX)/2, (minY+maxY)/2);
                            k++;
                        }
                    }
                }
            }
            if (k == 0) {
                return new Point[0];
            }
            sort_points();
        }
        return p;
    }
}