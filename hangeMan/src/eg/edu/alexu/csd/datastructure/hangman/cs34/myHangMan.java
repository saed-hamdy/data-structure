package eg.edu.alexu.csd.datastructure.hangman.cs34;

import eg.edu.alexu.csd.datastructure.hangman.IHangman;

public class myHangMan implements IHangman {

    public int j, counter, nwrong, n, nSecrit, maxmam;
    public String[] str;
    public static char data[]; // = new char [50];
    // public static int j,counter,nwrong,n ,nSecrit,maxmam;
    public int Mistake = 0;
    static String pass;

    @Override
    public void setDictionary(String[] words) {
        n = words.length;
        // TODO Auto-generated method stub

        str = words;

    }

    @Override
    public String selectRandomSecretWord() {
        // TODO Auto-generated method stub

        if (n == 0) {
            return null;
        }
        int random = (int) (Math.random() * n);
        nSecrit = str[random].length();
        data = new char[nSecrit];
        for (j = 0; j < nSecrit; j++) {
            data[j] = '-';
        }
        pass = str[random];
        return pass;
    }

    @Override
    public String guess(Character c) {
        int i;
        counter = 0;
        if (c == null) {
            return new String(data);
        } else {
            for (i = 0; i < nSecrit; i++) {
                if (pass.toLowerCase().charAt(i) == Character.toLowerCase(c)) {
                    data[i] = pass.charAt(i);
                    counter++;
                }

            }
            if (counter == 0) {
                nwrong++;
            }

            if (nwrong < maxmam) {
                return new String(data);
            } else {
                return null;
            }
        }

        // return null;
    }

    @Override
    public void setMaxWrongGuesses(Integer max) {
        // TODO Auto-generated method stub

        if (max == null) {
            maxmam = 0;
        } else
            maxmam = max;

    }

}