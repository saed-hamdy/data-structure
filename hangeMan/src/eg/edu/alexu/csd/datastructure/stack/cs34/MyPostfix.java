package eg.edu.alexu.csd.datastructure.stack.cs34;

import java.util.Scanner;

import eg.edu.alexu.csd.datastructure.stack.IExpressionEvaluator;

public class MyPostfix implements IExpressionEvaluator {
    Scanner in;

    public int isOperator(char c) {
        switch (c) {
        case '-':
        case '+':
            return 1;
        case '/':
        case '*':
            return 2;
        default:
            return 0;
        }
    }

    @Override
    public String infixToPostfix(String expression) {
        // TODO Auto-generated method stub

        MyStack post = new MyStack();
        StringBuilder value = new StringBuilder();
        StringBuilder ex = new StringBuilder();
        int operators = 0, operations = 0, flag = 0;
        String y;
        int n = expression.length();
        for (int i = 0; i < n; i++) {
            char ch = expression.charAt(i);
            switch (ch) {
            case '+':
                ex.append(" + ");
                operations++;
                flag = 0;
                break;
            case '-':
                ex.append(" - ");
                operations++;
                flag = 0;
                break;
            case '*':
                ex.append(" * ");
                operations++;
                flag = 0;
                break;
            case '/':
                ex.append(" / ");
                operations++;
                flag = 0;
                break;
            case ')':
                post.pop();
                ex.append(" )");
                flag = 0;
                break;
            case '(':
                ex.append("( ");
                flag = 0;
                post.push("(");

                break;
            case ' ':
                break;
            default:
                ex.append(ch);
                if (flag == 0) {
                    operators++;
                    flag = 1;
                }

            }
        }

        if (operators - operations != 1 || !post.isEmpty()) {
            throw new RuntimeException();
        }
        expression = (new String(ex));
        String[] s = expression.split(" ");
        char test;
        int x = 0;
        String st;
        // char c = s[s.length - 1].charAt(0);
        // if (isOperator(test) || isOperator(c)) {
        // throw new RuntimeException();
        // }
        int k = s.length;

        for (int i = x; i < k; i++) {

            st = s[i];
            test = st.charAt(0);
            switch (test) {
            case '+':
            case '-':
                if (!post.isEmpty()) {
                    switch (isOperator(post.peek().toString().charAt(0))) {
                    case 1:
                        value.append(post.pop().toString());
                        value.append(" ");
                        break;
                    case 2:
                        value.append(post.pop().toString());
                        value.append(" ");
                        if (!post.isEmpty() && isOperator(post.peek().toString().charAt(0)) == 1) {
                            value.append(post.pop().toString());
                            value.append(" ");
                        }
                        break;
                    default:

                    }
                    post.push(test);
                } else
                    post.push(test);
                break;

            case '*':
            case '/':
                if (!post.isEmpty()) {
                    y = post.peek().toString();
                    if (y.charAt(0) == '*' || y.charAt(0) == '/') {
                        value.append(y);
                        value.append(" ");
                        post.pop();
                    }
                }
                post.push(test);
                break;
            case '(':
                post.push("(");
                break;
            case ')':
                y = post.peek().toString();
                while (y.charAt(0) != '(') {
                    value.append(y);
                    value.append(" ");
                    post.pop();
                    y = post.peek().toString();
                }
                post.pop();
                break;
            default:
                value.append(st);
                value.append(" ");

            }

        }
        int z = post.size();
        for (int i = 0; i < z; i++) {
            value.append(post.pop().toString());
            value.append(" ");
        }
        // value.append(post.pop().toString());
        String ss = new String(value);
        if (ss.charAt(ss.length() - 1) == ' ') {
            ss = ss.substring(0, ss.length() - 1);
        }

        return ss;
    }

    @Override
    public int evaluate(String expression) {

        StringBuilder ex = new StringBuilder();
        for (int i = 0; i < expression.length(); i++) {
            char ch = expression.charAt(i);
            switch (ch) {
            case '+':
                ex.append(" + ");
                break;
            case '-':
                ex.append(" - ");
                break;
            case '/':
                ex.append(" / ");
                break;
            case '*':
                ex.append(" * ");
                break;
            default:
                ex.append(ch);
                break;

            }

        }
        expression = new String(ex);
        expression = expression.trim();
        String[] s = expression.split(" +");
        MyStack ev = new MyStack();

        char test;
        int sum, z = s.length;
        for (int i = 0; i < z; i++) {
            String st = s[i];
            test = st.charAt(0);
            if (isOperator(test) == 0) {
                ev.push(Float.parseFloat(s[i]));
            } else {
                switch (test) {
                case '+':
                    ev.push((float) ev.pop() + (float) ev.pop());
                    break;
                case '-':
                    ev.push(-((float) ev.pop() - (float) ev.pop()));
                    break;
                case '*':
                    ev.push((float) ev.pop() * (float) ev.pop());
                    break;
                case '/':
                    float x = (float) ev.pop(), y = (float) ev.pop();

                    ev.push(y / x);
                    break;

                default:
                    ;

                }
            }
        }

        if (ev.size() != 1)
            return 0;

        sum = (int) (1 * (float) ev.pop());

        return sum;
    }

}