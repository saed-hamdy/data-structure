package eg.edu.alexu.csd.datastructure.stack.cs34;

public class myMain {
    public static void main(String[] args) {
        MyPostfix testMe = new MyPostfix();
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < 1000000; i++) {
            string = string.append("1+");
        }
        string = string.append("1");
        long start = System.currentTimeMillis();
        String ev = testMe.infixToPostfix(string.toString());
        long first = System.currentTimeMillis();
        testMe.evaluate(ev);
        long second = System.currentTimeMillis();
        System.out.println(first - start);
        System.out.println(second - first);
        System.out.println((double) (second - start) / 1000);
    }
}
