package eg.edu.alexu.csd.datastructure.stack.cs34;

import eg.edu.alexu.csd.datastructure.linkedList.cs34.Node;
import eg.edu.alexu.csd.datastructure.linkedList.cs34.SingleLinkedList;
import eg.edu.alexu.csd.datastructure.stack.IStack;

public class MyStack implements IStack {

    SingleLinkedList stack = new SingleLinkedList();

    @Override
    public void add(int index, Object element) {
        // TODO Auto-generated method stub
        stack.add(stack.size() - index, element);

    }

    @Override
    public Object pop() {
        // TODO Auto-generated method stub

        Node top = stack.head;
        stack.remove(0);
        return top.value;

    }

    @Override
    public Object peek() {
        // TODO Auto-generated method stub

        Node top = stack.head;
        return top.value;
    }

    @Override
    public void push(Object element) {
        // TODO Auto-generated method stub
        stack.add(0, element);

    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub

        return stack.isEmpty();
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return stack.size();
    }
}